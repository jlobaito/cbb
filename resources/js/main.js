// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {

    initSlider: function () {
        $('.flexslider').flexslider({
            animation: "slide"
        });
    },

    // Initializers
    common: {
        init: function () {

        },
        finalize: function () {

        }
    },

    has_slider: {
        init: function () {
            APPNAME.initSlider();
        },
        finalize: function () {

        }
    }
};

UTIL = {
    fire: function (func, funcname, args) {
        var namespace = APPNAME; // indicate your obj literal namespace here

        funcname = (funcname === undefined) ? 'init' : funcname;
        if (func !== '' && namespace[func] && typeof namespace[func][funcname] == 'function') {
            namespace[func][funcname](args);
        }
    },
    loadEvents: function () {
        var bodyId = document.body.id;

        // hit up common first.
        UTIL.fire('common');

        // do all the classes too.
        $.each(document.body.className.split(/\s+/), function (i, classnm) {
            UTIL.fire(classnm);
            UTIL.fire(classnm, bodyId);
        });
        UTIL.fire('common', 'finalize');
    }
};

$(document).ready(UTIL.loadEvents);

/* ================================
                  ISOTOPE
=================================== */

var ddData = [

                    { text:"Alabama" },
                    { text:"Alaska" },
                    { text:"Arizona" },
                    { text:"Arkansas" },
                    { text:"California" },
                    { text:"Colorado" },
                    { text:"Connecticut" },
                    { text:"Delaware" },

                    { text:"Florida" },
                    { text:"Georgia" },
                    { text:"Hawaii" },
                    { text:"Idaho" },
                    { text:"Illinois" },
                    { text:"Indiana" },
                    { text:"Iowa" },
                    { text:"Kansas" },
                    { text:"Kentucky" },
                    { text:"Louisiana" },
                    { text:"Maine" },
                    { text:"Maryland" },
                    { text:"Massachusetts" },
                    { text:"Michigan" },
                    { text:"Minnesota" },
                    { text:"Mississippi" },
                    { text:"Missouri" },
                    { text:"Montana" },
                    { text:"Nebraska" },
                    { text:"Nevada" },
                    { text:"New Hampshire" },
                    { text:"New Jersey" },
                    { text:"New Mexico" },
                    { text:"New York" },
                    { text:"North Carolina" },
                    { text:"North Dakota" },
                    { text:"Ohio" },
                    { text:"Oklahoma" },
                    { text:"Oregon" },
                    { text:"Pennsylvania" },
                    { text:"Rhode Island" },
                    { text:"South Carolina" },
                    { text:"South Dakota" },
                    { text:"Tennessee" },
                    { text:"Texas" },
                    { text:"Utah" },
                    { text:"Vermont" },
                    { text:"Virginia" },
                    { text:"Washington" },
                    { text:"West Virginia" },
                    { text:"Wisconsin" },
                    { text:"Wyoming" },
                    
                ];

$('#myDropdown').ddslick({
    data: ddData,
    selectText: "Select a State",
    showSelectedHTML: false,
    onSelected: function (data) {
        defaultTarget: "_blank"
        var str = data.selectedData.text

        console.log(str);
        $(".desc").hide();
        $('#' + str).show();
    }
});


/*STATE BEEF COUNCILS*/
$(function () {
    $('.select').change(function () {
        var selected = $(this).find(':selected').text();
        //alert(selected);
        $(".desc").hide();
        $('#' + selected).show();
    });
});